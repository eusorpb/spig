#!/usr/bin/env python

import copy
import math
import os
import os.path
import sys
import tempfile
import gzip

import numpy

# import features

import cppfeatures # Make sure you do the following commands if this can't be loaded:
# cd features
# python setup.py build
# sudo python setup.py install

PROGRAM_NAME = 'satgen'

class Instance:
    def __init__(self):
        self.origPath = ''
        self.clauses = [[]] # Note: 1-indexed
        self.variables = [[]] # Note: 1-indexed
        self.features = {}
        self.hornIndecies = []
        self.avgClauseSize = 0
        self.stdevClauseSize = 0

        # The average number of clauses each variable is found in (rounded down)
        self.avgVarInClauses = 0
        self.avgClauseGraphOutDeg = 0
        self.stdevClauseGraphOutDeg = 0
        self.clauseOutDegree = []
        self.clauseOut = []

    # Returns a tuple (pos, neg) counting the number of variable-clause assignments that are positive and the number that are negated
    def posNegVariables(self):
        pos = 0
        neg = 0
        for cc in self.clauses:
            for vv in cc:
                if vv > 0:
                    pos += 1
                elif vv < 0:
                    neg += 1
        return (pos, neg)

    def computeAvgVarInClauses(self):
        self.avgVarInClauses = int(numpy.mean([len(clauses) for clauses in self.variables]))

    def computeAvgClauseDegree(self):
        outDegree = [0] * (len(self.clauses) - 1)
        self.clauseOut = [[] for cc in self.clauses]
        for ii, clause in enumerate(self.clauses[1:]):
            for vv in clause:
                for connClause in self.variables[abs(vv)]:
                    if ii < connClause:
                        outDegree[ii] += 1
                        self.clauseOut[ii].append(connClause)
        self.avgClauseGraphOutDeg = numpy.mean(outDegree)
        self.stdevClauseGraphOutDeg = numpy.std(outDegree)
        self.clauseOutDegree = [0] + outDegree

    def pruneUnusedVariables(self):
        unusedVars = sorted([vv for vv, clauses in enumerate(self.variables[1:], 1) if len(clauses) == 0])
        print "Pruning {0} useless variables: {1}".format(len(unusedVars), unusedVars)
        for uv in reversed(unusedVars):
            del self.variables[uv]
        for clause in self.clauses:
            for ii, vv in enumerate(clause):
                for uv in unusedVars:
                    if abs(vv) > uv:
                        clause[ii] -= int(math.copysign(1.0, vv))
                    else:
                        break

    def sanityCheck(self):
        sane = True
        for ii, cc in enumerate(self.clauses):
            for jj, vv in enumerate(cc):
                if vv >= len(self.variables):
                    print " !!! Sanity check failed (self.clauses): variable {0} at position {1} in clause {2} >= {3}.".format(vv, jj, ii)
                    sane = False
                if int(math.copysign(ii, vv)) not in self.variables[abs(vv)]:
                    print " !!! Sanity check failed (variable -- clause sanity): variable {0} at position {1} in clause {2} not matched. Clause {2}: {4}; clause list for variable {0}: {3}".format(vv, jj, ii, self.variables[abs(vv)], cc)
                    sane = False

        for ii, vv in enumerate(self.variables):
            for jj, cc in enumerate(vv):
                if cc >= len(self.clauses):
                    print " !!! Sanity check failed (self.variables): clause {0} at position {1} in clause {2} >= {3}.".format(cc, jj, ii)
                    sane = False
                if int(math.copysign(ii, cc)) not in self.clauses[abs(cc)]:
                    print " !!! Sanity check failed (clause -- variable sanity): clause {0} at position {1} in clause {2} not matched. Variable list for clause: {3}".format(cc, jj, ii, vv)
                    sane = False
        return sane

    def __deepcopy__(self, memo):
        retobj = Instance()
#         retobj.__dict__.update(self.__dict__)
        retobj.origPath = copy.deepcopy(self.origPath, memo)
        retobj.clauses = copy.deepcopy(self.clauses, memo)
        retobj.variables = copy.deepcopy(self.variables, memo)
        retobj.features = copy.deepcopy(self.features, memo)
        retobj.hornIndecies = copy.deepcopy(self.hornIndecies, memo)
        retobj.avgClauseSize = copy.deepcopy(self.avgClauseSize, memo)
        retobj.stdevClauseSize = copy.deepcopy(self.stdevClauseSize, memo)
        retobj.avgVarInClauses = copy.deepcopy(self.avgVarInClauses, memo)
        return retobj

# Loads the DIMACS CNF file located at path and returns an Instance
# Note that this function reduces all variable IDs by 1 in order to index from 0.
def loadInstance(path, useGzip = False):
    if not os.path.exists(path):
        raise Exception("Instance not found: {0}\n".format(path))
    inst = Instance()
    inst.origPath = path
    openCmd = gzip.open if useGzip else open
    with openCmd(path, 'r') as fp:
        inComments = True
        line = ''
        while inComments: # Ignore comments
            line = fp.readline()
            inComments = line.startswith('c')
        if line.startswith('p'): # Problem size
            numvars, numclauses = map(int, line.split()[-2:])
            inst.variables = [[] for ii in xrange(numvars + 1)]
            inst.clauses = [[] for ii in xrange(numclauses + 1)]
        else:
            raise Exception("Malformatted DIMACS file: Expected problem size after comments, received: {0}".format(line))
        for cc, line in enumerate(fp, 1): # CNF
            varrs = sorted(map(int, line.split()[:-1])) # remove 0 at end
            inst.clauses[cc] = varrs
            inst.avgClauseSize += len(varrs)
            numPos = 0
            for vv in varrs:
                inst.variables[abs(vv)].append(int(math.copysign(cc, vv)))
                if vv > 0:
                    numPos += 1
            if numPos <= 1:
                inst.hornIndecies.append(cc)
        inst.avgClauseSize /= len(inst.clauses)
        for cc in inst.clauses[1:]:
            inst.stdevClauseSize += (len(cc)-inst.avgClauseSize)*(len(cc)-inst.avgClauseSize)
        inst.stdevClauseSize = math.sqrt(inst.stdevClauseSize / len(inst.clauses) )
    inst.computeAvgVarInClauses()
    inst.computeAvgClauseDegree()
    return inst

# Outputs an instance in the DIMACS CNF format to the specified path. Comments
# will be automatically escaped for the CNF format.
def outputInstance(inst, path, comments = '', useGzip = False):
    openCmd = gzip.open if useGzip else open
    if useGzip:
        path = path + ".gz"
    with openCmd(path, 'w') as fp:
        fp.write("c Instance generated by {0}, based off of instance: {1}\n".format(PROGRAM_NAME, inst.origPath))
        if comments:
            for cc in comments.split('\n'):
                fp.write("c {0}\n".format(cc))
        fp.write(instCnfStr(inst))
        
def instCnfStr(inst):
    retStr = "p cnf {0} {1}\n".format(len(inst.variables) - 1, len(inst.clauses) - 1)
    retStr += ''.join(["{0} 0\n".format(" ".join(map(str, cc))) for cc in inst.clauses[1:]])
    return retStr

def instCnfStream(inst, fp):
    fp.write("p cnf {0} {1}\n".format(len(inst.variables) - 1, len(inst.clauses) - 1))
    for cc in inst.clauses[1:]:
        fp.write("{0} 0\n".format(" ".join(map(str, cc))))

# Computes features using a temporary file and calling out to SATZilla's feature generator
def computeInstanceFeaturesSZ(inst):
    fhndl, tmpfile = tempfile.mkstemp('satgen')
    outputInstance(inst, tmpfile)
    feats = features.computeBaseFeatures(tmpfile)
    if os.path.exists(tmpfile):
        os.close(fhndl)
        os.unlink(tmpfile)
    inst.features = feats
    return feats

def computeInstanceFeatures(inst):
    featNames = ["vars-over-clauses", "vcg_var_mean", "vcg_var_std", "vcg_var_min", "vcg_var_max", "vcg_var_spread", "vcg_cls_mean", "vcg_cls_std", "vcg_cls_min", "vcg_cls_max", "vcg_cls_spread", "pnr_var_mean", "pnr_var_std", "pnr_var_min", "pnr_var_max", "pnr_var_spread", "pnr_cls_mean", "pnr_cls_std", "pnr_cls_min", "pnr_cls_max", "pnr_cls_spread", "binary", "trinary", "horn_mean", "horn_std", "horn_min", "horn_max", "horn_spread", "horn "]
    feats = { featNames[ii] : feat for ii,feat in enumerate(cppfeatures.compute(instCnfStr(inst))) }
    inst.features = feats
    return feats

# import threading
# def computeInstanceFeatures(inst):
# #     tmpfile = tempfile.mkstemp('satgen')[1]
#     tmpfname = "/tmp/satgen.pipe"
#     if os.path.exists(tmpfname):
#         os.unlink(tmpfname)
#     try:
#         fifo = os.mkfifo(tmpfname)
#         lock = threading.Lock()
#         fcthread = FeatureCompThread(tmpfname, lock)
#         fcthread.start()
#         outputInstance(inst, tmpfname)
#         fcthread.join()
#         inst.features = fcthread.features
#     except Exception:
#         exc_type, exc_value, exc_traceback = sys.exc_info()
#         traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stdout)
#         sys.stderr.write("Failed to create shared pipe to compute features. Exiting.\n")
#         sys.exit(1)
#     if os.path.exists(tmpfname):
#         os.unlink(tmpfname)
#     return inst.features

class Pool:
    def __init__(self):
        self.instances = []
        self.means = {}
        self.stdevs = {}

    def updateMeansStdevs(self, modInst):
        self.means = {}
        self.stdevs = {}
        print "Precomputing means and stdevs for the pool (including the instance to modify)."
        for fkey in self.instances[0].features.keys():
            featVals = [inst.features[fkey] for inst in self.instances + [modInst]]
            self.means[fkey] = numpy.mean(featVals)
            self.stdevs[fkey] = numpy.std(featVals)
        print "Done updating means and stdevs."

    # clobbers all current instances and adds the instances, insts, and calls out to compute their features
    def setInstances(self, insts, modInst):
        self.instances = insts
        for ii, inst in enumerate(self.instances):
            print "Computing base features for pool instance {0}: {1}".format(ii, inst.origPath)
            inst.features = computeInstanceFeatures(inst)
            inst.pruneUnusedVariables()
        computeInstanceFeatures(modInst)
        self.updateMeansStdevs(modInst)

if __name__ == "__main__":
    print " === TESTING instance.py start === "
    if len(sys.argv) < 2:
        sys.stderr.write("Test usage: ./instance.py <instances>\n")
        sys.exit(1)
    insts = []
    for inst in sys.argv[1:]:
        cnf = loadInstance(inst)
#         outputInstance(cnf, 'test.cnf', "testing\ntesting2\ntesting3")
        insts.append(cnf)
    pool = Pool()
    pool.setInstances(insts)
#     for inst in pool.instances:
#         print inst.features
    print " === TESTING instance.py end === "


