#!/usr/bin/env python

import instance

from itertools import chain
import numpy
import math
import sys

# input: receiver (Instance), and clauses added (list of indices of the clauses in giver that should be removed)
# output: no return, but receiver is changed
def destroyInstance(receiver, clausesRemoved):
    print ">>> Starting to disassemble the instance"
    variablesUsed = []
    
    clausesRemoved = list(set(map(abs, clausesRemoved))) 
    
    ## Get the variables that will be affected by this change
    for cc in clausesRemoved:
        variablesUsed.extend(receiver.clauses[cc])
    
    variablesUsed = map(abs, variablesUsed)
    variablesUsed = list(set(variablesUsed))
    variablesUsed = sorted(variablesUsed)
    
    print "Variables of interest: {0}".format(variablesUsed)
    print "Removing {0} clauses: {1}".format(len(clausesRemoved), clausesRemoved)
    print "Before: # clauses = {0}".format(len(receiver.clauses))

    ## Swap the clauses so that the ones that need to be removed are closer to the end
    for cc in reversed(sorted(clausesRemoved, cmp=lambda xx,yy: cmp(abs(xx), abs(yy)))):
        index_1 = abs(cc)
        index_2 = len(receiver.clauses)-1
        #print "Touching Clauses {0} and {1}".format(index_1, index_2)
        clause_1 = receiver.clauses[index_1]
        clause_2 = receiver.clauses[index_2]
        if index_1 == index_2:
            ## Remove any mention of clauses to be removed from the variables
            for (ii, vv) in enumerate(clause_1):
                receiver.variables[abs(vv)] = [xx for xx in receiver.variables[abs(vv)] if abs(xx)!=index_1]
            del receiver.clauses[-1]
            continue
        
        ## Remove any mention of clauses to be removed from the variables
        for (ii, vv) in enumerate(clause_1):
            receiver.variables[abs(vv)] = [xx for xx in receiver.variables[abs(vv)] if abs(xx)!=index_1]
        ## Correct the clause indices in all the affect variables
        for (ii, vv) in enumerate(clause_2):
            receiver.variables[abs(vv)] = [int(math.copysign(index_1,xx)) if abs(xx)==index_2 else xx for xx in receiver.variables[abs(vv)]]
        ## Swap the clause into the now unused slot
        receiver.clauses[index_1] = clause_2
        ## Remove the last clause which is currently duplicated
        del receiver.clauses[-1]
    
    print "After: # clauses = {0}".format(len(receiver.clauses))
    
    # Remove variables that no longer occur in the wild (translation from yuri speak: variables that are no longer in any clauses)
    for vv in reversed(sorted(variablesUsed, cmp=lambda xx,yy: cmp(abs(xx), abs(yy)))):
        index_1 = abs(vv)
        index_2 = len(receiver.variables)-1
        #print "Touching variables {0} and {1}".format(index_1, index_2)
        vars_1 = receiver.variables[index_1]
        vars_2 = receiver.variables[index_2]
        if len(vars_1) == 0:
            if index_1 == index_2:
                del receiver.variables[-1]
                continue
            
            # Swap the clause into the now unused slot
            receiver.variables[index_1] = vars_2
            # Correct the variable indices in all the affected variables
            for (ii, cc) in enumerate(vars_2):
                receiver.clauses[abs(cc)] = [int(math.copysign(index_1,xx)) if abs(xx)==index_2 else xx for xx in receiver.clauses[abs(cc)]]
            # Remove the last clause which is currently duplicated
            del receiver.variables[-1]
    
    largestClause = 0
    for vv, item in enumerate(receiver.variables):
        if vv == 0:
            continue
        if len(item) == 0:
            print "ERROR: Variable {0} has no clauses".format(vv)
            sys.exit(1)
        largestClause = max(largestClause, max(item), abs(min(item)))
    
    print "Largest clause referenced by a variable: {0}".format(largestClause)
    
    ## Find all unary clauses and remove them
    badClauses = []
    for cc, item in enumerate(receiver.clauses):
        if len(item) <= 1:
            badClauses.append(cc)
    # if( len(badClauses) > 0 ):
    # destroyInstance(receiver, badClauses)
    
    pass



