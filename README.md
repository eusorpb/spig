## Overview

Structure-preserving Instance Generation (SpIG) is a technique for generating new instances based on the structures of existing ones. It is meant for expanding datasets of industrial instances with similar instances for use in solver design, algorithm configuration and algorithm selection. The key difference between SpIG and other instance generation paradigms is its focus on generating real-world instances with similar machine learning features to existing instances. This ensures the instances it generates share the same properties as industrial instances.

The current version of SpIG supports generating SAT and MaxSAT instances. We are hoping to add more types of problems in the future (such as mixed-integer programming). If you are interested in helping with this, please contact us. Furthermore, this code still has a number of bugs and may crash unexpectedly. We apologize for the inconvenience. Please let us know if this occurs.

We have submitted a paper to the LION 10 conference about the SpIG system. A slightly extended version of the paper is available in this repository (spig-extended.pdf)

## Installation

First, install the following dependencies:
* Python 2.7.x
* The numpy package for Python

Then, you need to install a SAT feature computation package provided in the features/ directory.

    cd features
    python setup.py install

You may need root access (sudo) to install the package, depending on your installation of python. The c++ compiler "g++" is required for compiling the feature computation code.

Finally, if you wish to generate SAT instances, you need to install the Lingeling solver from [http://fmv.jku.at/lingeling]. Then copy or symlink the solver in the main SpIG directory, or use the --lingelingCmd option to point to your installation of Lingeling.

## Usage examples

We provide two examples for generating instances. The relevant industrial SAT instances are provided in the repository in the example/orig directory. Note that if you wish to generate MaxSAT instances, simply pass the "--no_hardness_check" flag. All generated instances are output to the example/gen directory. Note also that with the --gzip flag, all input and output is passed through gzip (as industrial SAT and MaxSAT instances tend to be rather large). If this is not desired, simply remove this flag.

### Example 1:

This example works quickly, but usually only generates a few instances as it is hard to match the structure of the keyfind instances. If more instances are desired, pass the flag --iterations 100 to run 100 iterations instead of the default of 10. Alternatively, you can execute SpIG multiple times with different random seeds to get more instances.

    python spig.py --gzip --outputDir example/gen/ --outputPrefix aes_24_4_keyfind_5 example/orig/aes_24_4_keyfind_5.cnf.gz example/orig/aes_32_3_keyfind_1.cnf.gz example/orig/aes_64_1_keyfind_1.cnf.gz

### Example 2:

    python spig.py --gzip --outputDir example/gen/ --outputPrefix UR-15-10p0 example/orig/UR-15-10p0.cnf.gz example/orig/dated-10-13-u.cnf.gz example/orig/dated-5-13-u.cnf.gz

## Citing this work

Our work is currently under review for publication at the LION 10 conference. Once this process is finished, a citation will be provided.

## License

This code is licensed under the MIT license as follows:

Copyright (c) 2013 - 2015 Kevin Tierney, Yuri Malitsky and Marius Merschformann

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.