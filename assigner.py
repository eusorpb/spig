#!/usr/bin/env python

import math
import random

import instance, features, clustercomparators

class Assigner(object):
    def __init__(self):
        pass
    def assign(self, inst, prevClauses, replacedClauses):
        pass

# Builds a dict: [variable] -> (clause, position in clause, pos/neg: 1.0 / -1.0)
def buildClauseDict(inst, replacedClauses):
    varClauses = {}
    for rc in replacedClauses:
        for ii, vv in enumerate(inst.clauses[rc]):
            varClauses.setdefault(abs(vv), []).append((rc, ii, math.copysign(1.0, vv)))
    return varClauses

# Assumes the variables in replacedClauses are now set, and this updates the
# structures in replacedClauses to reflect these changes.
def fixInstance(inst, prevClauses, replacedClauses):
    for ii, pclause in enumerate(prevClauses):
        for vv in pclause:
            inst.variables[abs(vv)] = [cc for cc in inst.variables[abs(vv)] if abs(cc) != replacedClauses[ii]]
    for rclause in replacedClauses:
        for vv in inst.clauses[rclause]:
            inst.variables[abs(vv)] = math.copysign(1.0, vv) * rclause

class RandomAssigner(Assigner):
    def __init__(self):
        pass

    def assign(self, inst, prevClauses, replacedClauses):
        # for each variable, determine clauses and whether pos/neg
        varClauses = buildClauseDict(inst, replacedClauses) 

        # make random assignments (without replacement)
        asgn = random.sample(xrange(1, len(inst.variables)), len(varClauses))

        for jj, (vv, clauses) in enumerate(varClauses.iteritems()):
            for (cc, ii, pp) in clauses:
                inst.clauses[cc][ii] = int(pp * asgn[jj])

        fixInstance(inst, prevClauses, replacedClauses)

# Note that this performs a non-incremental local search
class LocalSearchAssigner(Assigner):
    def __init__(self, pool):
        self.pool = pool

    def assign(self, inst, prevClauses, replacedClauses):
        varClauses = buildClauseDict(inst, replacedClauses) 

        # Choose a random assignment as above
        asgn = { vv : aa for vv,aa in zip(varClauses.keys(), random.sample(xrange(1, len(inst.variables)), len(varClauses))) }

        for vv, clauses in varClauses.iteritems():
            for (cc, ii, pp) in clauses:
                inst.clauses[cc][ii] = int(pp * asgn[vv])

        # compute current features and begin LS (currently an SA, consider an ILS)
        curFeats = instance.computeInstanceFeatures(inst)

        stdevcmp = clustercomparators.StdDevComparator()
        l2cmp = clustercomparators.L2Comparator()
        temp = 1e2 # TODO parameterize with argparse
        minTemp = 1e-5
        saAlpha = 0.99
        curObj = l2cmp.l2calc(inst, self.pool)

        # Loop until SA convergence or the modified instance is within the
        # cluster in all dimensions
        while temp > minTemp or not stdevcmp.accept(inst, self.pool):
            # LS neighborhood: Pick a random variable and re-assign it
            rndVar = varClauses.keys()[random.randint(1, len(varClauses) - 1)]
            rasgn = random.randint(1, len(inst.variables) - 1)
            while rasgn == asgn[rndVar]: # Prevent clause redundancy!
                rasgn = random.randint(1, len(inst.variables) - 1)
            for (cc, ii, pp) in varClauses[rndVar]:
                inst.clauses[cc][ii] = int(pp * rasgn)
            newFeats = instance.computeInstanceFeatures(inst)
            newObj = l2cmp.l2calc(inst, self.pool)
            try:
                metropolis = math.exp(-(newObj - curObj) / temp)
            except:
                temp = -1
                break
            if metropolis > random.random():
                improving = "improving" if newObj < curObj else "worsening"
                print " (SA) Accepted {4} change; old: {0:.5f}; new: {1:.5f}; diff: {5:.5f} t: {2:.3f}; exp: {3:.3f}".format(curObj, newObj, temp, metropolis, improving, newObj - curObj)
                asgn[rndVar] = rasgn # Accept change
                curObj = newObj
            else: # Reject change
                for (cc, ii, pp) in varClauses[rndVar]:
                    inst.clauses[cc][ii] = int(pp * asgn[rndVar])
                print " (SA) Rejected change; old: {0:.5f}; new: {1:.5f}; diff: {5:.5f} t: {2:.3f}; exp: {3:.3f}".format(curObj, newObj, temp, metropolis, None, newObj - curObj)
            temp *= saAlpha
        if temp > minTemp:
            print " (SA) SA completed; found assignment of variables within cluster."
        else:
            print " (SA) SA completed; unable to find assignment of variables within cluster."

        # fix the instance based on the current variable assignment
        fixInstance(inst, prevClauses, replacedClauses)
        








