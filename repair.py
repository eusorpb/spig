#!/usr/bin/env python

import copy
from itertools import chain
import numpy
import math
import sys
import random

import instance

class IndexDist:
    def __init__(self, index, dist):
        self.index = index
        self.dist = dist

    def __lt__(self, other):
        return self.dist < other.dist


def varFeatures(inst, varrs):
    numFeatures = 3
    numVars = len(varrs)
    
    features = numpy.zeros((numVars, numFeatures))
    
    for vv in xrange(1,numVars):
        if varrs[vv] >= len(inst.variables):
            print "varrs[{0}] = {1} out of range (len: {2})".format(vv, varrs[vv], len(inst.variables))
        features[vv,0] = 10.0*len(inst.variables[varrs[vv]])/len(inst.clauses)
        features[vv,1] = 10.0*sum(xx > 0 for xx in inst.variables[varrs[vv]])/len(inst.variables[varrs[vv]])
        features[vv,2] = 1.0*sum(len(inst.clauses[int(abs(xx))]) for xx in inst.variables[varrs[vv]])/len(inst.variables[varrs[vv]])
    
    return features

def remove_values_from_list(the_list, val):
    return [value for value in the_list if value != val]

def clauseIsDuped(clause, inst):
    clause.sort()
    for jj, dc in enumerate(inst.clauses):
        if len(dc) == len(clause):
            same = True
            for kk, vv in enumerate(sorted(dc)):
                if vv != clause[kk]:
                    same = False
                    break
            if same:
#                 print "Found duplicate clause for clause {0}/{1} (dupe: {3}): {2}".format(ii, len(clausesAdded), clause, jj)
                return True
    return False

# input: giver (Instance), receiver (Instance), and clauses added (list of indices of the clauses in giver that should be added). origVariables is the original number of variables in the instance -- if the current number of variables is less than this, some new variables will be added instead of making existing assignments
# Setting dorandom to True ignores the variable features and just does things randomly
# output: no return, but receiver is changed
def repairInstance(giver, receiver, clausesAdded, origVariables, dorandom = False):
    print ">>> Starting repair of the receiver instance"
    variablesUsed = []

    for cc in clausesAdded:
        variablesUsed.extend(giver.clauses[cc])
    variablesUsed = map(abs, variablesUsed)
    variablesUsed = list(set(variablesUsed))
    variablesUsed = sorted(variablesUsed)
    
    g_features = varFeatures(giver, variablesUsed)
    r_features = varFeatures(receiver, xrange(len(receiver.variables)))
    
    newLabels  = [-1] *  len(variablesUsed)
    
    variableDeficit = max(0, origVariables - len(receiver.variables))
    reversalProb = variableDeficit / float(len(variablesUsed))
    trivialVar = {}
    # When new variables are required, do a basic check as to whether or not
    # the variable to be replaced would be trivial to solve or not if replaced
    # with a new variable
    # Currently, this means that the variable must be positive in at least one
    # clause and negative in at least one clause
    if reversalProb > 0: 
        hasPos = {}
        hasNeg = {}
        for cc in clausesAdded:
            for vv in giver.clauses[cc]:
                if vv < 0:
                    hasNeg[vv] = True
                else:
                    hasPos[vv] = True
        for vv in variablesUsed:
            trivialVar[vv] = hasNeg.get(vv, False) and hasPos.get(vv, False)

    numVarsToAdd = 0
    for vv in xrange(len(variablesUsed)):
        distances = [numpy.linalg.norm(g_features[vv,:]-r_features[ii,:]) for ii in xrange(len(receiver.variables))]
        bestIndex = distances.index(min(distances[1:]))
        if dorandom:
            bestIndex = random.randint(1, len(receiver.variables) - 1)
        
        if bestIndex not in newLabels:
            newLabels[vv] = distances.index(min(distances[1:]))
        else:
#             dists = []
            bestIndex = -1
            bestDist = 2*max(distances[1:])
            for ii, item in enumerate(distances):
#                 if ii not in newLabels and ii > 0:
#                     dists.append(IndexDist(ii, item))
                if item < bestDist and ii not in newLabels and ii > 0:
                    bestIndex = ii 
                    bestDist = item
#             dists.sort()
#             bestIndex = dists[0].index
#             if len(dists) > 1 and random.random() < 0.1: # TODO if this helps at all, parameterize!
                # With low probability, do not take the very best option. Rather, pick a random index from the top 5 (TODO parameterize? use probability to pick something worse?)
#             bestIndex = (random.choice(dists[:10])).index # TODO Testing: Pick a random variable from the top 10 choices
            newLabels[vv] = bestIndex
        # With some probability reverse the previous decision, as long as the variable is in more than 1 clause
        if not trivialVar.get(vv, True) and random.random() < reversalProb:
            newLabels[vv] = len(receiver.variables) + numVarsToAdd;
            numVarsToAdd += 1 # can't add the variable yet or it breaks the linalg norm
#     if numVarsToAdd > 0:
#         print "AAA Adding {0} variables. Deficit was {1}; probability of adding: {2}".format(numVarsToAdd, variableDeficit, reversalProb)
    for ii in xrange(numVarsToAdd):
        receiver.variables.append([])

    for ii, cc in enumerate(clausesAdded):
        clause = copy.deepcopy(giver.clauses[cc])
        for jj, vv in enumerate(clause):
            index = variablesUsed.index(abs(vv))
            clause[jj] = int(math.copysign(newLabels[index], vv))
        if not clauseIsDuped(clause, receiver):
            for vv in clause:
                receiver.variables[abs(vv)].append(int(math.copysign(len(receiver.clauses), vv)))
            receiver.clauses.append(clause)

    print "   newLabels: {0}".format(newLabels)

