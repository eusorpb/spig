#!/usr/bin/env python

import copy
import clustercomparators, instance, instchanger, features, assigner

# Performs a search for similar instances with features similar to the
# instances in pool.
# Parameters:
#  mod -- instance (class) to modify
#  pool -- pool (class) of instances to use for modifications
#  maxIters -- maximum number of times to make changes before stopping
def search(mod, pool, clusterComp = clustercomparators.StdDevComparator(), changer = instchanger.AlmostRandomChanger(), asgner = assigner.RandomAssigner(), maxIters = 1000):
    instsOutput = 0
    itr = 0
    curMod = copy.deepcopy(mod)
    while itr < maxIters: # TODO Add other stopping conditions
        print "> Starting iteration {0}".format(itr)
        modPrime = copy.deepcopy(curMod)
        (modPrime, prevClauses, replacedClauses) = changer.change(modPrime, pool)
        if replacedClauses:
            asgner.assign(modPrime, prevClauses, replacedClauses)
            # TODO Perform LS to optimize assignments for each clause in replacedClauses
        else:
            continue # no change took place
        instance.computeInstanceFeatures(modPrime) # TODO Will not be necessary if the LS can do some incremental updating
        if clusterComp.accept(modPrime, pool):
            curMod = copy.deepcopy(modPrime)
            changer.provideFeedback(True)
            print " >>> Accepted changes to incumbent; outputting new instance."
            instsOutput += 1
            # TODO Output an instance
        else:
            changer.provideFeedback(False)
            print " >>> Rejected changes to incumbent."
        print "< Ending iteration {0}".format(itr)
        itr += 1
    print " << After search: Output {0} instances.".format(instsOutput)




