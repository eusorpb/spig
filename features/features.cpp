#include <Python.h>

#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdio>
#include <vector>
#include <list>
#include <math.h>
#include <algorithm>

using namespace std;

struct VARIABLE {   
	//list<int> pos_clause;
	//list<int> neg_clause;
	int numPos;
	int size;
	int horn;
	
	VARIABLE() : numPos(0), size(0), horn(0) {}
	void add( int clause, int sign ){
		if( sign > 0 ){
			numPos++;
			//pos_clause.push_back( clause );
		}
		else{
			//neg_clause.push_back( clause );
		}
		size++;
	}
};

struct CLAUSE {   
	list<int> pos_vars;
	list<int> neg_vars;
	int numPos;
	int size;
	
	CLAUSE() : numPos(0), size(0){}
	void add( int var, int sign ){
		if( sign > 0 ){
			numPos++;
			pos_vars.push_back( var );
		}
		else{
			neg_vars.push_back( var );
		}
		size++;
	}
};



int featuresFromStr(const char* fstr, vector<double>& feats) {
    string line;
    stringstream ss;

    ss << fstr;

//     const char *ext = strrchr(argv[2], '.');
//     const char *wcnf = "wcnf";    
//     
//     int isWCNF = 0;
//     if( (strncmp((ext+1), wcnf, 4) == 0) ){ isWCNF = 1; }
    int isWCNF = 0; // for now, just completely disable wcnf

	float eps = 0.00001;
	
	int readHeader = 0;
	unsigned int numVars;
	unsigned int numClauses;
	unsigned int maxWeight;
	
	float vcg_var_mean = 0;
	float vcg_var_std = 0;
	float vcg_var_min, vcg_var_max, vcg_var_spread;
	float vcg_cls_mean = 0;
	float vcg_cls_std = 0;
	float vcg_cls_min, vcg_cls_max, vcg_cls_spread;
	float pnr_var_mean = 0;
	float pnr_var_std = 0;
	float pnr_var_min, pnr_var_max, pnr_var_spread;
	float pnr_cls_mean = 0;
	float pnr_cls_std = 0;
	float pnr_cls_min, pnr_cls_max, pnr_cls_spread;
	float horn_mean = 0;
	float horn_std = 0;
	float horn_min = 0;
	float horn_max = 0;
	float horn_spread = 0;
	float unary = 0;
	float binary = 0;
	float trinary = 0;
	float horn = 0;
	float num_soft = 0;
	float soft_mean = 0;
	float soft_std = 0;
	float soft_max = 0;
	float soft_min = 0;
	
	vector<VARIABLE>	myVars;
	vector<CLAUSE>		myClauses;
	vector<float>		mySoftWeights;
 	
	int clause_id = 0;
    // read the data from the file
    while ( ss.good() )
    {
        getline (ss,line);
        char* str = &line[0];
        
        // skip over empty lines
        if( line.size() == 0 ){ continue; }
        
        if( readHeader == 0 ){
            if( str[0] == 'p' ){
                if( isWCNF == 0 ){
                    sscanf (str,"%*s %*s %u %u",&numVars, &numClauses);
                }
                else{
                    sscanf(str,"%*s %*s %u %u %u",&numVars, &numClauses, &maxWeight);
                    //printf("maxWeight: %u\n", maxWeight );
                }
                readHeader = 1;
                
                myVars.resize(numVars);
                myClauses.resize(numClauses);
            }
        }
        else{
            char* pch;
            pch = strtok (str," ");
            int value, variable, sign;
            unsigned int weight;
            if( isWCNF == 0 ){
                weight = 0;
            }
            else{
                sscanf (pch,"%u",&weight);
                if( weight < maxWeight ){
                    num_soft += 1;
                    mySoftWeights.push_back(weight);
                }
            }
            
            
            pch = strtok (NULL, " ");
            while (pch != NULL){
                sscanf (pch,"%d",&value);
                
                if( value == 0 ){ break; }
                
                sign = (value > 0 ? 1 : -1);
                variable = value*sign;
                
                myVars[variable - 1].add( clause_id,  sign );
                myClauses[clause_id].add( variable-1, sign );
                    
                pch = strtok (NULL, " ");
            }
            
            if( myClauses[clause_id].size == 1 ){ unary += 1; }
            if( myClauses[clause_id].size <= 2 ){ binary += 1; }
            if( myClauses[clause_id].size <= 3 ){ trinary += 1; }
            if( myClauses[clause_id].numPos <= 1 ){ 
                horn += 1; 
                for( list<int>::iterator it = myClauses[clause_id].pos_vars.begin(); it != myClauses[clause_id].pos_vars.end(); it++ ){ myVars[*it].horn++; }
                for( list<int>::iterator it = myClauses[clause_id].neg_vars.begin(); it != myClauses[clause_id].neg_vars.end(); it++ ){ myVars[*it].horn++; }
            }
            
            myClauses[clause_id].pos_vars.clear();
            myClauses[clause_id].neg_vars.clear();
            
            clause_id++;
        }
    }
    
    if( isWCNF == 0 ){
        soft_mean = 0;
        soft_std = 0;
        soft_min = 0;
        soft_max = 0;
    }
    else{
        soft_mean = 0;
        soft_std = 0;
        for( int ss = 0; ss < (int)mySoftWeights.size(); ss++ ){
            soft_mean += mySoftWeights[ss];
            
            if( ss == 0 || mySoftWeights[ss] < soft_min ){ soft_min = mySoftWeights[ss]; }
            if( ss == 0 || mySoftWeights[ss] > soft_max ){ soft_max = mySoftWeights[ss]; }
        }
        if( mySoftWeights.size() > 0 ){
            soft_mean /= (1.0*mySoftWeights.size());
            for( int ss = 0; ss < (int)mySoftWeights.size(); ss++ ){
                soft_std += (soft_mean - mySoftWeights[ss]) * (soft_mean - mySoftWeights[ss]);
            }
            if( soft_std > eps ){ soft_std = sqrt(soft_std/(1.0*mySoftWeights.size())); }
            else{ soft_std = 0; }
        }
    }
    
    numClauses = clause_id;
    numVars = 0;
    for( int vv = 0; vv < (int)myVars.size(); vv++ ){
        if( myVars[vv].size > 0 ){ numVars++; }
    }
    
    // compute the features of the instance
    for( int cc = 0; cc < (int)myClauses.size(); cc++ ){
        if( myClauses[cc].size == 0 ){ continue; }
        
        float _size = myClauses[cc].size / (1.0*numVars);
        if( cc == 0 || _size < vcg_cls_min ){ vcg_cls_min = _size; }
        if( cc == 0 || _size > vcg_cls_max ){ vcg_cls_max = _size; }
        vcg_cls_mean += _size;
        
        float _pnr = 0.5 + ((2.0*myClauses[cc].numPos - myClauses[cc].size) / (2.0*myClauses[cc].size));
        if( cc == 0 || _pnr < pnr_cls_min ){ pnr_cls_min = _pnr; }
        if( cc == 0 || _pnr > pnr_cls_max ){ pnr_cls_max = _pnr; }
        pnr_cls_mean += _pnr;
    }
    
    vcg_cls_mean /= 1.0*numClauses;
    pnr_cls_mean /= 1.0*numClauses;
    horn /= 1.0*numClauses;
    unary /= 1.0*numClauses;
    binary /= 1.0*numClauses;
    trinary /= 1.0*numClauses;
    
    vcg_cls_spread = vcg_cls_max - vcg_cls_min;
    pnr_cls_spread = pnr_cls_max - pnr_cls_min;
    
    for( int vv = 0; vv < (int)myVars.size(); vv++ ){
        if( myVars[vv].size == 0 ){ continue; }
        
        float _size = myVars[vv].size / (1.0*numClauses);
        if( vv == 0 || _size < vcg_var_min ){ vcg_var_min = _size; }
        if( vv == 0 || _size > vcg_var_max ){ vcg_var_max = _size; }
        vcg_var_mean += _size;
        
        float _pnr = 0.5 + ((2.0*myVars[vv].numPos - myVars[vv].size) / (2.0*myVars[vv].size));
        if( vv == 0 || _pnr < pnr_var_min ){ pnr_var_min = _pnr; }
        if( vv == 0 || _pnr > pnr_var_max ){ pnr_var_max = _pnr; }
        pnr_var_mean += _pnr;
        
        float _horn = myVars[vv].horn / (1.0*numClauses);
        if( vv == 0 || _horn < horn_min ){ horn_min = _horn; }
        if( vv == 0 || _horn > horn_max ){ horn_max = _horn; }
        horn_mean += _horn;
    }
    vcg_var_mean /= 1.0*numVars;
    pnr_var_mean /= 1.0*numVars;
    horn_mean /= 1.0*numVars;
    
    vcg_var_spread = vcg_var_max - vcg_var_min;
    pnr_var_spread = pnr_var_max - pnr_var_min;
    horn_spread = horn_max - horn_min;
    
    for( int cc = 0; cc < (int)myClauses.size(); cc++ ){
        if( myClauses[cc].size == 0 ){ continue; }
        
        float _size = myClauses[cc].size / (1.0*numVars);
        vcg_cls_std += (vcg_cls_mean-_size)*(vcg_cls_mean-_size);
        
        float _pnr = 0.5 + ((2.0*myClauses[cc].numPos - myClauses[cc].size) / (2.0*myClauses[cc].size));
        pnr_cls_std += (pnr_cls_mean-_pnr)*(pnr_cls_mean-_pnr);
    }
    if( vcg_cls_std > eps && vcg_cls_mean > eps ){ vcg_cls_std = sqrt(vcg_cls_std/(1.0*numClauses))/vcg_cls_mean; }
    else{ vcg_cls_std = 0; }
    if( pnr_cls_std > eps && vcg_cls_mean > eps ){ pnr_cls_std = sqrt(pnr_cls_std/(1.0*numClauses))/pnr_cls_mean; }
    else{ pnr_cls_std = 0; } 
    
    for( int vv = 0; vv < (int)myVars.size(); vv++ ){
        if( myVars[vv].size == 0 ){ continue; }
        
        float _size = myVars[vv].size / (1.0*numClauses);
        vcg_var_std += (vcg_var_mean-_size)*(vcg_var_mean-_size);
        
        float _pnr = 0.5 + ((2.0*myVars[vv].numPos - myVars[vv].size) / (2.0*myVars[vv].size));
        pnr_var_std += (pnr_var_mean-_pnr)*(pnr_var_mean-_pnr);
        
        float _horn = myVars[vv].horn / (1.0*numClauses);
        horn_std += (horn_mean-_horn)*(horn_mean-_horn);
    }
    if( vcg_var_std > eps && vcg_var_mean > eps ){ vcg_var_std = sqrt(vcg_var_std/(1.0*numVars))/vcg_var_mean; }
    else{ vcg_var_std = 0; }
    if( pnr_var_std > eps && pnr_var_mean > eps ){ pnr_var_std = sqrt(pnr_var_std/(1.0*numVars))/pnr_var_mean; }
    else{ pnr_var_std = 0; }
    if( horn_std/(1.0*numVars) > eps && horn_mean > eps ){ horn_std = sqrt(horn_std/(1.0*numVars))/horn_mean; }
    else{ horn_std = 0; }
		   
    feats.resize(29);
    feats[0] = numVars / (1.0*numClauses);
    feats[1] = vcg_var_mean;
    feats[2] = vcg_var_std;
    feats[3] = vcg_var_min;
    feats[4] = vcg_var_max;
    feats[5] = vcg_var_spread;
    feats[6] = vcg_cls_mean;
    feats[7] = vcg_cls_std;
    feats[8] = vcg_cls_min;
    feats[9] = vcg_cls_max;
    feats[10] = vcg_cls_spread;
    feats[11] = pnr_var_mean;
    feats[12] = pnr_var_std;
    feats[13] = pnr_var_min;
    feats[14] = pnr_var_max;
    feats[15] = pnr_var_spread;
    feats[16] = pnr_cls_mean;
    feats[17] = pnr_cls_std;
    feats[18] = pnr_cls_min;
    feats[19] = pnr_cls_max;
    feats[20] = pnr_cls_spread;
    feats[21] = binary;
    feats[22] = trinary;
    feats[23] = horn_mean;
    feats[24] = horn_std;
    feats[25] = horn_min;
    feats[26] = horn_max;
    feats[27] = horn_spread;
    feats[28] = horn ;
    
	return 0;
}

static PyObject* cppfeatures_compute(PyObject* self, PyObject* args) {
    PyObject* cnf;
    if(!PyArg_ParseTuple(args, "S", &cnf)) {
        return NULL;
    }
    vector<double> feats;
    featuresFromStr(PyString_AsString(cnf), feats);
    PyObject* plo = PyList_New(feats.size());
    for(size_t ii = 0; ii < feats.size(); ++ii) {
        PyList_SetItem(plo, ii, PyFloat_FromDouble(feats[ii]));
    }
    return plo;
}

static PyMethodDef CppFeaturesMethods[] = {
    {"compute", cppfeatures_compute, METH_VARARGS, "Computes the features of a given instance, given in DIMACS CNF as a string."},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initcppfeatures(void) {
    (void)Py_InitModule("cppfeatures", CppFeaturesMethods);
}

int main(int argc, char** argv){
    Py_SetProgramName(argv[0]);
    Py_Initialize();
    initcppfeatures();

}


