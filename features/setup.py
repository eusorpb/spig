from distutils.core import setup, Extension

module1 = Extension('cppfeatures',
                    sources = ['features.cpp'])

setup (name = 'CPPFeatures',
       version = '1.0',
       description = 'Computes SAT features for a DIMACS CNF instance.',
       ext_modules = [module1])

