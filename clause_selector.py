#!/usr/bin/env python

import sys
import random

# TODO Balance +/- ratios for repair clause selection!

# Selects clauses from an instance in the pool.
# Returns a tuple: (giver, list of clause IDs to move from the giver to the receiver)
def selectClauses(pool):
    giver = random.choice(pool.instances)
    clauses = variableHeuristic(giver) if random.random() < 0.5 else clauseHeuristic(giver)
    return (giver, list(set(clauses)))

def selectClausesDestroy(receiver):
    return variableHeuristic(receiver) if random.random() < 0.5 else clauseHeuristic(receiver)

def selectClausesDestroyRandom(receiver):
    return variableRandom(receiver) if random.random() < 0.5 else clauseRandom(receiver)

def variableRandom(giver):
    return random.choice(giver.variables)

def variableHeuristic(giver):
    eligibleVars = [ii for ii, clauses in enumerate(giver.variables) if len(clauses) == giver.avgVarInClauses]
    fails = 0
    while not eligibleVars: # This is not at all efficient, but I don't expect it to ever happen.
        print "Warning: Failure to find any variables in {0} clauses (+/- {1}).".format(giver.avgVarInClauses, fails)
        fails += 1
        eligibleVars = [ii for ii, clauses in enumerate(giver.variables) if abs(len(clauses) - giver.avgVarInClauses) <= fails]
    
    var = random.choice(eligibleVars)

    return giver.variables[var]

def clauseRandom(giver):
    return giver.clauseOut[random.randint(1, len(giver.clauses) - 1)]
    
def clauseHeuristic(giver):
    foundClause = False
    tries = 0

    maxTries = 100
    stdevFactor = 0.25
    while not foundClause and tries < maxTries:
        randClause = random.randint(1, len(giver.clauses) - 1)
        foundClause = abs(giver.clauseOutDegree[randClause] - giver.avgClauseGraphOutDeg ) < stdevFactor * giver.stdevClauseGraphOutDeg 
        tries += 1
    if tries < maxTries:
        return giver.clauseOut[randClause]
    else:
        print " !!! Failed to find clause with correct out degree in clause heuristic"
        sys.exit(1)


