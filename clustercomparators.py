#!/usr/bin/env python

import math
# from scipy.spatial import Delaunay

# Stores functions for determining whether an instance is within its cluster
# and should be accepted by the local search as a good change.

EPS = 1e-3

# To define a new comparator, simply override this object and implement
# accept(self, inst, pool)
class Comparator(object):
    def __init__(self):
        pass
    def accept(self):
        pass

# Checks whether all features are within numStdDevs standard deviations of the
# mean for each feature individually. That is, if even a single feature is
# outside the std dev this method returns false.
# TODO Note that if the instance to modify is outside of two standard deviations on any feature this process may not accept many changes.
class StdDevComparator(Comparator):
    def __init__(self, numStdDevs = 3.0):
        self.numStdDevs = numStdDevs

    def accept(self, inst, pool):
        for fkey, fval in inst.features.iteritems():
            diff = abs(fval - pool.means[fkey])
            # TODO some features have absolutely no variation in the pool, but we cannot seem to match that...
            stdev = self.numStdDevs * pool.stdevs[fkey] if abs(pool.stdevs[fkey]) > EPS else EPS
            if diff > stdev:
                print " >>> (Rejecting change) Feature {0}; (mod,pm,ps): ({1},{2},{3}); abs diff: {4} > {5}".format(fkey,fval,pool.means[fkey],pool.stdevs[fkey],diff,stdev)
                return False
        return True

class L2Comparator(Comparator):
    def __init__(self, eps = 1e-5):
        self.eps = eps

    def accept(self, inst, pool):
        return l2calc(inst,pool) > self.eps

    def l2calc(self, inst, pool):
        sqsum = 0.0
        for fkey, fval in inst.features.iteritems():
            sqsum += (fval - pool.means[fkey]) ** 2
        return math.sqrt(sqsum)

# class DelaunayComparator(Comparator):
#     # Creates a delaunay triangulation of the given points (should be the feature vectors of the instances)
#     def __init__(self, noise_eps = 1e-3):
#         self.noise_eps = noise_eps
#         self.triangles = None
# 
#     def accept(self, inst, pool):
#         if not self.triangles:
#             points = [ii.features for ii in pool.instances]
#             self.triangles = Delaunay(points)
#         # TODO Probably too strict; consider hyperplane distance hull.plane_distance
#         if triangles.find_simplex(inst.features) < 0:
#             print " >>> (Rejecting change) Point lies outside of the convex hull."
#             return False
#         return True



