#!/usr/bin/env python

# Runs the SATGen instance generator

import argparse
import os
import sys
import random
import copy
from datetime import datetime
import numpy
import hardness

import instance, repair, clause_selector, clustercomparators, destroy

def main():
    class Args:
        pass
    parser = argparse.ArgumentParser(description='SAT/MaxSAT instance generation based on features.')
    parser.add_argument('--seed', type=int, default=0, help='Random seed initialization.')
    parser.add_argument('--outputDir', type=str, default='.', help='Output directory for generated instances.')
    parser.add_argument('--outputFreq', type=int, default=1, help='Sets the frequency for outputting a CNF. When this is 1, every modified instance is output; 2 means every other instance and so on.')
    parser.add_argument('--outputPrefix', type=str, default='', help='Instance output prefix.')
    parser.add_argument('--iterations', type=int, default=10, help='Number of repair/destroy iterations to perform.')
    parser.add_argument('--clauseLB', type=float, default=0.995, help='If the number of clauses falls below clauseLB * origClauses, the repair method will be run until the number of clauses is above this threshold.')
    parser.add_argument('--gzip', action='store_true', help='All IO is performed through gzip to reduce filesizes.')
    parser.add_argument('--trivialTimeout', type=int, default=30, help='Timeout in sec. for instance triviality check.')
#     parser.add_argument('--acceptance', type=str, default='stdev', help='Acceptance criterion. stdev = 3 standard deviations away from the cluster center; ch = convex hull of pool features')
    parser.add_argument('--random', action='store_true', help='Enables random generation mode, which disregards our heuristics and pieces together instances randomly.')
    parser.add_argument('--no_hardness_check', action='store_true', help='Disables the hardness checking to use SpIG for MaxSAT instances.')
    parser.add_argument('--lingelingCmd', type=str, default='./lingeling', help='The command to call the lingeling solver.')

    parser.add_argument('inst', type=str, help='The instance to modify.')
    parser.add_argument('poolinst', type=str, nargs='+', help='Instance(s) in the pool.')

    parser.add_argument('--stdevCmp', type=float, default=3.0, help='Number of standard deviations around the mean to allow for acceptance of an instance.')



    args = Args()
    parser.parse_args(namespace=args)

    if not os.path.exists(args.outputDir):
        os.mkdir(args.outputDir)

    outPrefix = ""
    if args.outputPrefix:
        outPrefix = "{0}-".format(args.outputPrefix)
        print "Outputting generated instances to directory {0} with prefix {1}.".format(args.outputDir, args.outputPrefix)
    else:
        print "Outputting generated instances to directory {0} with no prefix.".format(args.outputDir)

#     args = sys.argv
#     if len(args) < 2:
#         sys.stderr.write("Usage: ./satgen.py <instance to modify> <instance pool>\n")
#         sys.exit(1)

    print "Using random seed: {0}".format(args.seed)
    random.seed(args.seed)

    print "Loading instance to modify ({0}).".format(args.inst)
    modInst = instance.loadInstance(args.inst, args.gzip)
    print "Instance to modify loaded. Pruning unused variables"
    modInst.pruneUnusedVariables()
    insts = []
    for ii, aa in enumerate(args.poolinst):
        print "Loading instance for pool: ({0})".format(aa)
        insts.append(instance.loadInstance(aa, args.gzip))
    pool = instance.Pool()
    pool.setInstances(insts, modInst)

    satgen(outPrefix, args, modInst, pool)

def satgen(outPrefix, args, modInst, pool, run_id = 0):
    l2cmp = clustercomparators.L2Comparator()
    origDist = l2cmp.l2calc(modInst, pool)

    origVars = len(modInst.variables)
    origClauses = len(modInst.clauses)

#     (origPos, origNeg) = modInst.posNegVariables()
#     print "PNV ", origPos, origNeg, "%.3f" % (origPos / float(origNeg)), (origPos + origNeg), origVars, len(modInst.clauses)

    outputList = []
    acceptance = clustercomparators.StdDevComparator(args.stdevCmp)
    print "Instance to modify distance from cluster center: {0}".format(origDist)
    print "Performing {0} repair/destroy iterations.".format(args.iterations)
    clauseThresh = int(max(0.1 * origClauses, origClauses * float(args.clauseLB)))
    for ii in xrange(args.iterations):
        print "\n>>> Beginning destroy/repair iteration {0}".format(ii)
        revertCpy = copy.deepcopy(modInst)

        # Destroy a piece of the instance
        modInst.computeAvgVarInClauses()
        modInst.computeAvgClauseDegree()
        if args.random:
            dclauses = clause_selector.selectClausesDestroyRandom(modInst)
        else:
            dclauses = clause_selector.selectClausesDestroy(modInst)
        destroy.destroyInstance(modInst, dclauses)

        # Perform a repair (add something to the instance)
        ranOnce = False
        while not ranOnce or len(modInst.clauses) < clauseThresh:
            if ranOnce:
                print ">> Number of clauses ({1}) below clause threshold ({0}); running repair again".format(clauseThresh, len(modInst.clauses))
            giver, clauses = clause_selector.selectClauses(pool)
            repair.repairInstance(giver, modInst, clauses, origVars, args.random)
            ranOnce = True

        if ii % args.outputFreq == 0:
            acceptChange = True
            if not args.no_hardness_check:
                acceptChange = hardness.hardnessCheck(modInst, args.trivialTimeout, args.lingelingCmd)
            if acceptChange:
                print " >>> Hardness-check: Instance is assumed to be hard enough."
                instance.computeInstanceFeatures(modInst)
                acceptChange = acceptance.accept(modInst, pool)
                withinClusterStr = "Yes" if acceptChange else "No"
                print " >>> (Output instance is within cluster in all dimensions? {0})".format(withinClusterStr)
                distToCenter = l2cmp.l2calc(modInst, pool)
                print " >>> (Euclidean distance to cluster center: {0})".format(distToCenter)
            else:
                print " >>> Hardness-check: Instance is trivial."
            if acceptChange:
                instName = "{0}gen-{1}.cnf".format(outPrefix, ii)
                outputList.append(instName)
                print " >>> Output instance {0} to {1}".format(instName, args.outputDir)
                comments = "Instances in instance pool: \n\t{0}\nInstance within cluster? {1}\nDistance to cluster center: {2}\nAverage distance: {3}".format("\n\t".join(args.poolinst), withinClusterStr, distToCenter, numpy.mean(pool.means.values()))
                instance.outputInstance(modInst, "{0}/{1}".format(args.outputDir, instName), comments, args.gzip)
            else:
                print " >>> Changes are too disruptive. Reverting destroy/repair."
                modInst = revertCpy

#     instance.computeInstanceFeatures(modInst)
#     print "Is the new instance within the old cluster? {0}".format("Yes" if acceptance.accept(modInst, pool) else "No" )
#     print "Euclidean distance of original instance from cluster center: {0}".format(origDist)
#     print "Euclidean distance of modified instance from cluster center: {0}".format(l2cmp.l2calc(modInst, pool))

#     print len(modInst.variables), len(modInst.clauses)

    if len(outputList) == 0:
        run_id += 1
        if run_id < 5:
            clustercomparators.EPS *= 10
            outputList.extend(satgen(outPrefix, args, modInst, pool, run_id))
        else:
            return []

    if run_id  == 0: # Don't print this message out for each recursive operation
        # Print a summary of the parameters for reproducibility
        with open("{0}/{1}-gen.info".format(args.outputDir, args.outputPrefix), 'w') as fp:
            fp.write("Experimented completed on at {0}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
            fp.write("Parameters: \n")
            for pname, pvalue in args.__dict__.iteritems():
                fp.write("\t{0}: {1}\n".format(pname, pvalue))
            fp.write("Output instances: {0}\n".format(", ".join(outputList)))
    else:
        return outputList
    
if __name__ == "__main__":
    main()
