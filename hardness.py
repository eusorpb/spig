#!/usr/bin/env python

import sys
import math
import os
import signal
from subprocess import Popen, PIPE, call
import threading
import instance
import cppfeatures
import io
import StringIO

# class enabling calls to subprocesses with time-outs
class Command(object):
    def __init__(self, instance, timeout, lingelingcmd):
        self.cmd = "{1} -t {0} -q".format(timeout, lingelingcmd)
        self.instanceToAnalyze = instance
        self.process = None
        self.timeout = timeout

    def run(self):
        def target():
            # open the process and redirect a stream of the instance to its stdin
            self.process = Popen(self.cmd, shell=True, stdin=PIPE, preexec_fn=os.setsid)
            instance.instCnfStream(self.instanceToAnalyze, self.process.stdin)
            self.process.communicate()

        # start the test-thread
        thread = threading.Thread(target=target)
        thread.start()

        # wait for the thread for the given timeout - if the timeout expires the test is assumed to be successful
        thread.join(self.timeout)
        if thread.is_alive():
            self.success = True
            #self.process.kill()
            os.killpg(self.process.pid, signal.SIGKILL)
            thread.join()
        else:
            self.success = False
        print # Pushes out buffer so output of lingeling matches up

# checks whether an instance is trivially solvable
# returns: True: the instance is assumed to be hard enough, i.e. the timeout was completely consumed
#          False: otherwise
def hardnessCheck(instance, timeout, lingelingloc):
    command = Command(instance, timeout, lingelingloc)
    retCode = command.run()
    return command.success

def main():
    if len(sys.argv) != 2:
        print "Usage: ./hardness.py <instances to test>"
        sys.exit(1)

    print "Loading instance from file: {0} ...".format(sys.argv[1])

    instanceToTest = instance.loadInstance(sys.argv[1])

    print "Success!"
    print "Checking hardness of the instance ..."

    timeout = 5
    success = hardnessCheck(instanceToTest, timeout)

    if success == True:
        print "Hard enough!"
    else:
        print 'Too simple!'

if __name__ == "__main__":
    main()

